package com.devcamp.pizza362.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.pizza362.model.CVoucher;
import com.devcamp.pizza362.respository.IVoucherRespository;

import java.util.List;
import java.util.ArrayList;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CVoucherController {
    
    @Autowired
    IVoucherRespository pVoucherRespository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers(){

        try {
            List<CVoucher> listVouchers = new ArrayList<CVoucher>();

            pVoucherRespository.findAll().forEach(listVouchers::add);
            return new ResponseEntity<>(listVouchers,HttpStatus.OK);
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/voucher")
    public ResponseEntity<CVoucher> getVoucher(@RequestParam(name = "voucher_detail") String maVoucher){

        try {
            CVoucher vVoucher = pVoucherRespository.findByMaVoucher(maVoucher);
            if(vVoucher != null) {
                return new ResponseEntity<>(vVoucher,HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
