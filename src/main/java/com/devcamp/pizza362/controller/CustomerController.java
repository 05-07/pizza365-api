package com.devcamp.pizza362.controller;

import java.util.List;
import java.util.Set;

import com.devcamp.pizza362.model.CCustomer;
import com.devcamp.pizza362.model.COrder;
import com.devcamp.pizza362.respository.ICustomerRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {
    
    @Autowired
    ICustomerRespository pCustomerRespository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers(){
        
        try {
            List<CCustomer> pCustomers = new ArrayList<CCustomer>();

            pCustomerRespository.findAll().forEach(pCustomers::add);
            return new ResponseEntity<>(pCustomers,HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orders")
    public ResponseEntity<Set<COrder>> getOrderByCustomerId(@RequestParam(value = "customerId") Long customerId) {
        try {
            CCustomer vCustomer = pCustomerRespository.findByCustomerId(customerId);
            if (vCustomer != null) {
                return new ResponseEntity<>(vCustomer.getOrders(),HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        } catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
