package com.devcamp.pizza362.controller;

import java.util.List;

import com.devcamp.pizza362.model.CDrink;
import com.devcamp.pizza362.respository.IDrinkRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;

@RestController
@RequestMapping("/")
public class CDrinkController {
    
    @Autowired
    IDrinkRespository pDrinkRespository;
    @CrossOrigin
    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrinks(){

        try {
            List<CDrink> listDrinks = new ArrayList<CDrink>();

            pDrinkRespository.findAll().forEach(listDrinks::add);
            return new ResponseEntity<>(listDrinks,HttpStatus.OK);

        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
