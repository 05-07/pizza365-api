package com.devcamp.pizza362.controller;

import java.util.List;

import com.devcamp.pizza362.model.CMenu;
import com.devcamp.pizza362.respository.IMenuComboResponsitory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
@RestController
@RequestMapping("/")
@CrossOrigin
public class CMenuController {
    
    @Autowired
    IMenuComboResponsitory pMenuComboResponsitory;

    @GetMapping("menuCombo")
    public ResponseEntity<List<CMenu>> getAllMenu(){
        try {
            List<CMenu> allMenu = new ArrayList<>();

            pMenuComboResponsitory.findAll().forEach(allMenu::add);
            return new ResponseEntity<>(allMenu,HttpStatus.OK);
        } catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
}
