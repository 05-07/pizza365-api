package com.devcamp.pizza362.controller;

import java.util.List;
import java.util.Set;

import com.devcamp.pizza362.model.CProduct;
import com.devcamp.pizza362.model.CProductOrder;
import com.devcamp.pizza362.respository.IProductOrderRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.ArrayList;
@RestController
@CrossOrigin
@RequestMapping("/")
public class COrderProductController {
    
    @Autowired
    IProductOrderRespository pIProductOrderRespository;

    @GetMapping("/order-product")
    public ResponseEntity<List<CProductOrder>> getAllProductOrder(){

        try {

            List<CProductOrder> ListProductOrder = new ArrayList<CProductOrder>();

            pIProductOrderRespository.findAll().forEach(ListProductOrder::add);
            return new ResponseEntity<>(ListProductOrder,HttpStatus.OK);

        }
        catch (Exception ex) {
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/product")
    public ResponseEntity<Set<CProduct>> getProductByOrderId(@RequestParam(name = "orderId") Long orderId){

        try {

            CProductOrder pProducts = pIProductOrderRespository.findByOrderId(orderId);
            if(pProducts != null) {
                return new ResponseEntity<>(pProducts.getProducts(),HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(null,HttpStatus.NOT_FOUND);
            }
        
        } catch (Exception ex) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
}
