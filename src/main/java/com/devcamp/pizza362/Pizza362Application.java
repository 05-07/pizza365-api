package com.devcamp.pizza362;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza362Application {

	public static void main(String[] args) {
		SpringApplication.run(Pizza362Application.class, args);
	}

}
