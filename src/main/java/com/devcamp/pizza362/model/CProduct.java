package com.devcamp.pizza362.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "p_products")
public class CProduct {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "ten_san_pham")
    private String name;

    @Column(name = "loai_san_pham")
    private String type;

    @Column(name = "mau_san_pham")
    private String color;

    @Column(name = "gia_san_pham")
    private long price;

    @ManyToOne
    @JoinColumn(name = "orderId")
    @JsonBackReference
    private CProductOrder order_product;

    public CProduct() {

    }

    public CProduct(long id, String name, String type, String color, long price, CProductOrder order_product) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.color = color;
        this.price = price;
        this.order_product = order_product;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public CProductOrder getOrder_product() {
        return order_product;
    }

    public void setOrder_product(CProductOrder order_product) {
        this.order_product = order_product;
    }  
    
}
