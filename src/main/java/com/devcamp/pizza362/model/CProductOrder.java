package com.devcamp.pizza362.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "p_product_order")
public class CProductOrder {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long orderId;

    @Column(name = "ma_don_hang")
    private String orderCode;

    @Column(name = "co_pizza")
    private String pizzaSize;

    @Column(name = "loai_pizza")
    private String pizzaType;

    @Column(name = "ma_giam_gia")
    private String voucherCode;

    @Column(name = "gia_san_pham")
    private long price;

    @Column
    private long paid;
    
    @OneToMany(mappedBy = "order_product",cascade = CascadeType.ALL)
    @JsonManagedReference
    private Set<CProduct> products;

    public CProductOrder() {

    }

    public CProductOrder(long order_id, String orderCode, String pizzaSize, String pizzaType, String voucherCode, long price,
            long paid, Set<CProduct> products) {
        this.orderId = order_id;
        this.orderCode = orderCode;
        this.pizzaSize = pizzaSize;
        this.pizzaType = pizzaType;
        this.voucherCode = voucherCode;
        this.price = price;
        this.paid = paid;
        this.products = products;
    }

    public long getOrder_id() {
        return orderId;
    }

    public void setOrder_id(long order_id) {
        this.orderId = order_id;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(String pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public String getPizzaType() {
        return pizzaType;
    }

    public void setPizzaType(String pizzaType) {
        this.pizzaType = pizzaType;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPaid() {
        return paid;
    }

    public void setPaid(long paid) {
        this.paid = paid;
    }

    public Set<CProduct> getProducts() {
        return products;
    }

    public void setProducts(Set<CProduct> products) {
        this.products = products;
    }

    

}
