package com.devcamp.pizza362.model;

import javax.annotation.Generated;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu_combo")
public class CMenu {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "co_combo")
    private char menuSize;

    @Column(name = "duong_kinh")
    private String duongkinh;

    @Column(name = "so_luong_suon")
    private int suonNuong;

    @Column(name = "salad")
    private String salad;

    @Column(name = "so_luong_nuoc")
    private int nuocNgot;

    @Column(name = "gia_tien")
    private long price;

    
    public CMenu() {
        
    }


    public CMenu(char menuSize, String duongkinh, int suonNuong, String salad, int nuocNgot, long price) {
        this.menuSize = menuSize;
        this.duongkinh = duongkinh;
        this.suonNuong = suonNuong;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.price = price;
    }


    public char getMenuSize() {
        return menuSize;
    }


    public void setMenuSize(char menuSize) {
        this.menuSize = menuSize;
    }


    public String getDuongkinh() {
        return duongkinh;
    }


    public void setDuongkinh(String duongkinh) {
        this.duongkinh = duongkinh;
    }


    public int getSuonNuong() {
        return suonNuong;
    }


    public void setSuonNuong(int suonNuong) {
        this.suonNuong = suonNuong;
    }


    public String getSalad() {
        return salad;
    }


    public void setSalad(String salad) {
        this.salad = salad;
    }


    public int getNuocNgot() {
        return nuocNgot;
    }


    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }


    public long getPrice() {
        return price;
    }


    public void setPrice(long price) {
        this.price = price;
    }
}
