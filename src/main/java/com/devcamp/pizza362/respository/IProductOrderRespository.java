package com.devcamp.pizza362.respository;

import com.devcamp.pizza362.model.CProductOrder;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductOrderRespository extends JpaRepository<CProductOrder,Long>{

    CProductOrder findByOrderId(Long orderId);

}
