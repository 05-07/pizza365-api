package com.devcamp.pizza362.respository;

import com.devcamp.pizza362.model.CMenu;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IMenuComboResponsitory extends JpaRepository<CMenu,Integer>{
    
}
