package com.devcamp.pizza362.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.pizza362.model.COrder;
public interface IOrderRespository extends JpaRepository<COrder,Long>{
    
}
