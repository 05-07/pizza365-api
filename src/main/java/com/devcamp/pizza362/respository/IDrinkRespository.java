package com.devcamp.pizza362.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.pizza362.model.CDrink;
public interface IDrinkRespository extends JpaRepository<CDrink,Long>{
    
}
