package com.devcamp.pizza362.respository;

import com.devcamp.pizza362.model.CVoucher;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IVoucherRespository extends JpaRepository<CVoucher,Long>{
    
    CVoucher findByMaVoucher(String maVoucher);
    
}
