package com.devcamp.pizza362.respository;

import com.devcamp.pizza362.model.CProduct;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductRespository extends JpaRepository<CProduct,Long>{
    
}
