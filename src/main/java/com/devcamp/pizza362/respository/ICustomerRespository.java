package com.devcamp.pizza362.respository;

import com.devcamp.pizza362.model.CCustomer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ICustomerRespository extends JpaRepository<CCustomer,Long>{
    CCustomer findByCustomerId(Long customerId);
}
